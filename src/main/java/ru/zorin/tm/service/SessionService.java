package ru.zorin.tm.service;

import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.api.repository.ISessionRepository;
import ru.zorin.tm.api.service.IPropertyService;
import ru.zorin.tm.api.service.IServiceLocator;
import ru.zorin.tm.api.service.ISessionService;
import ru.zorin.tm.entity.Session;
import ru.zorin.tm.entity.User;
import ru.zorin.tm.error.invalid.InvalidIdException;
import ru.zorin.tm.error.invalid.InvalidLoginException;
import ru.zorin.tm.role.Role;
import ru.zorin.tm.util.HashUtil;
import ru.zorin.tm.util.SignatureUtil;

import java.util.List;

public class SessionService extends AbstractService<Session> implements ISessionService {

    private final IServiceLocator serviceLocator;

    private final ISessionRepository sessionRepository;

    public SessionService(ISessionRepository sessionRepository, IServiceLocator serviceLocator) {
        super(sessionRepository);
        this.serviceLocator = serviceLocator;
        this.sessionRepository = sessionRepository;
    }

    @Override
    public void close(@Nullable Session session) throws Exception {
        validate(session);
        sessionRepository.removeByUserId(session.getUserId());

    }

    @Override
    public void closeAll(@Nullable Session session) throws Exception {
        validate(session);
        sessionRepository.removeByUserId(session.getUserId());

    }

    @Override
    public @Nullable User getUser(@Nullable Session session) throws Exception {
        validate(session);
        return serviceLocator.getUserService().findById(session.getUserId());

    }

    @Override
    public @Nullable String getUserId(@Nullable Session session) throws Exception {
        validate(session);
        return session.getUserId();

    }

    @Override
    public @Nullable List<Session> getListSession(Session session) throws Exception {
        validate(session);
        return sessionRepository.findAllSession(session.getUserId());
    }

    @Override
    public @Nullable Session sign(@Nullable Session session) {
        if (session == null) return null;
        session.setSignature(null);
        final IPropertyService propertyService = serviceLocator.getPropertyService();
        final String salt = propertyService.getSessionSalt();
        final Integer cycle = propertyService.getSessionCycle();
        final String signature = SignatureUtil.sign(session, salt, cycle);
        session.setSignature(signature);
        return session;
    }

    @Override
    public boolean isValid(@Nullable Session session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void validate(@Nullable Session session) throws Exception {
        if (session == null) throw new NullPointerException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new NullPointerException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new NullPointerException();
        if (session.getTimestamp() == null) throw new NullPointerException();
        final Session temp = session.clone();
        if (temp == null) throw new NullPointerException();
        final String signatureSource = session.getSignature();
        final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new NullPointerException();
        if (sessionRepository.contains(session.getUserId())) ;
    }

    @Override
    public void validate(@Nullable Session session, @Nullable Role role) throws Exception {
        if (role == null) throw new NullPointerException();
        validate(session);
        final String userId = session.getUserId();
        final User user = serviceLocator.getUserService().findById(userId);
        if (user == null) throw new NullPointerException();
        if (user.getRole() == null) throw new NullPointerException();
        if (!role.equals(user.getRole())) throw new NullPointerException();
    }

    @Override
    public @Nullable Session open(@Nullable String login, @Nullable String password) {
        final Boolean check = checkDataAccess(login, password);
        if (!check) return null;
        final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) return null;
        final Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        sessionRepository.add(session);
        return sign(session);
    }

    @Override
    public boolean checkDataAccess(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) return false;
        final String passwordHash = HashUtil.md5(password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.getPassword());
    }

    @Override
    public void signOutByLogin(@Nullable String login) throws Exception {
        if (login == null || login.isEmpty()) throw new InvalidLoginException();
    }

    @Override
    public void signOutByUserId(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new InvalidIdException();
    }
}
