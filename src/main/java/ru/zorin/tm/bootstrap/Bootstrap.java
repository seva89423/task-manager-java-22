package ru.zorin.tm.bootstrap;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.zorin.tm.api.repository.*;
import ru.zorin.tm.api.service.*;
import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.endpoint.*;
import ru.zorin.tm.entity.User;
import ru.zorin.tm.error.invalid.InvalidCommandException;
import ru.zorin.tm.repository.*;
import ru.zorin.tm.role.Role;
import ru.zorin.tm.service.*;
import ru.zorin.tm.util.TerminalUtil;

import javax.xml.ws.Endpoint;
import java.util.*;

public final class Bootstrap implements IServiceLocator {

    private final PropertyService propertyService = new PropertyService();

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IAuthService authService = new AuthService(userService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final AuthEndpoint authEndpoint = new AuthEndpoint(authService);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IAdminService adminService = new AdminService(this);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final TaskEndpoint taskEndpoint = new TaskEndpoint(this);

    private final UserEndpoint userEndpoint = new UserEndpoint(this);

    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    private final AdminEndpoint adminEndpoint = new AdminEndpoint(this);

    private final AdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(this);

    private final ISessionRepository sessionRepository = new SessionRepository();

    private final ISessionService sessionService = new SessionService(sessionRepository, this);

    private final SessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    private final IDomainService domainService = new DomainService(userService, taskService, projectService);

    @NotNull
    private final String sessionEndpointUrl = "http://127.0.0.1:8080/SessionEndpoint?wsdl";

    @NotNull
    private final String taskEndpointUrl = "http://127.0.0.1:8080/TaskEndpoint?wsdl";

    @NotNull
    private final String projectEndpointUrl = "http://127.0.0.1:8080/ProjectEndpoint?wsdl";

    @NotNull
    private final String userEndpointUrl = "http://127.0.0.1:8080/UserEndpoint?wsdl";

    @NotNull
    private final String adminUserEndpointUrl = "http://127.0.0.1:8080/AdminUserEndpoint?wsdl";

    @NotNull
    private final String adminDataEndpointUrl = "http://127.0.0.1:8080/AdminEndpoint?wsdl";

    @NotNull
    private final String authDataEndpoint = "http://127.0.0.1:8080/AuthEndpoint?wsdl";

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();
    {
        initialize(commandService.getTermCommands());
    }

    private void initEndPoint() {
        Endpoint.publish("http://127.0.0.1:8080/SessionEndpoint?wsdl", sessionEndpoint);
        Endpoint.publish("http://127.0.0.1:8080/UserEndpoint?wsdl", userEndpoint);
        Endpoint.publish("http://127.0.0.1:8080/TaskEndpoint?wsdl", taskEndpoint);
        Endpoint.publish("http://127.0.0.1:8080/ProjectEndpoint?wsdl", projectEndpoint);
        Endpoint.publish("http://127.0.0.1:8080/AuthEndpoint?wsdl", authEndpoint);
        Endpoint.publish("http://127.0.0.1:8080/AdminEndpoint?wsdl", adminEndpoint);
        Endpoint.publish("http://127.0.0.1:8080/AdminUserEndpoint?wsdl", adminUserEndpoint);
    }
    private void initialize(List<AbstractCommand> commands) {
        for(AbstractCommand command: commands) registryCommand(command);
    }

    private void registryCommand(final AbstractCommand command) {
        if (command == null) return;
        command.setIServiceLocator(this);
        commands.put(command.name(), command);
        arguments.put(command.arg(), command);
    }

    private void initUser() {
        userService.create("test", "test", "test@email.com");
        userService.create("admin", "admin", Role.ADMIN);
    }

    private void intDemoData() {
        final User user = userService.findByLogin("test");
        if (user == null) return;
        final String userId = user.getId();
        taskService.create(userId,"Demo task", "Description 1");
        projectService.create(userId, "Demo project", "Description 2");
    }

    private void printEndpoint() {
        System.out.println(sessionEndpointUrl);
        System.out.println(taskEndpointUrl);
        System.out.println(projectEndpointUrl);
        System.out.println(userEndpointUrl);
        System.out.println(authDataEndpoint);
        System.out.println(adminDataEndpointUrl);
        System.out.println(adminUserEndpointUrl);
    }
    public void run(final String[] args) throws Exception {
        System.out.println("***WELCOME TO TASK MANAGER***");
        initUser();
        intDemoData();
        initEndPoint();
        printEndpoint();
        parseArg(args);
        inputCommand();
    }

    @SneakyThrows
    private void inputCommand() {
    while (true) {
        try {
            parseCommand(TerminalUtil.nextLine());
        } catch (Exception e) {
            System.err.println("[ERROR]");
            System.err.println(e.getMessage());
            }
        }
    }

    @SneakyThrows
    private void parseCommand(final String cmd) throws Exception {
        if (cmd == null || cmd.isEmpty()) return;
        final AbstractCommand command = commands.get(cmd);
        if (command == null) throw new InvalidCommandException();
        authService.checkRole(command.roles());
        command.execute();
    }

    @SneakyThrows
    private boolean parseArg(final String... args) throws Exception {
        if (args == null || args.length == 0) return false;
        for (String arg : args) validateArg(arg);
        return true;
    }

    @SneakyThrows
    private void validateArg(final String args) throws Exception {
        final AbstractCommand argument = arguments.get(args);
        if (arguments.get(args) == null) return;
        argument.execute();
    }

    public @NotNull IUserService getUserService() {
        return userService;
    }

    public @NotNull IAuthService getAuthService() {
        return authService;
    }

    public @NotNull ITaskService getTaskService() {
        return taskService;
    }

    public @NotNull IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IPropertyService getPropertyService() {
        return null;
    }

    @Override
    public ISessionService getSessionService() {
        return sessionService;
    }

    @Override
    public IAdminService getAdminService() {
        return adminService;
    }

    public @NotNull IDomainService getDomainService() {
        return domainService;
    }

    public @NotNull Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    public Collection<AbstractCommand> getArgs() {
        return arguments.values();
    }
}