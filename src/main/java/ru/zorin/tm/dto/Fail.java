package ru.zorin.tm.dto;

public class Fail extends Result {

    public Fail() {
        message = "";
        success = false;
    }

    public Fail(final Exception e) {
        success = false;
        if (e == null) return;
        message = e.getMessage();
    }

}
