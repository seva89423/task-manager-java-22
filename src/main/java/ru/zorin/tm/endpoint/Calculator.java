package ru.zorin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.entity.User;
import ru.zorin.tm.role.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;
import java.util.Date;

@WebService
public class Calculator {

    @Nullable
    @WebMethod
    public int sum(
            @WebParam(name = "a") int a,
            @WebParam(name = "b") int b
    ) {
        return a + b;
    }

    @NotNull
    public Date getData() {
        return new Date();
    }

    @NotNull
    public User getUser() {
        final User user = new User();
        user.setLogin("James_Bond");
        user.setRole(Role.ADMIN);
        user.setEmail("seva89423@gmail.com");
        return user;
    }

    public static void main(String[] args) {
        final Calculator calculator = new Calculator();
        Endpoint.publish("http://localhost:8080/Calculator?wsdl", calculator);
    }



}
