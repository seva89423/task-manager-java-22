package ru.zorin.tm.constant;

public class DataConst {

    public static final String FILE_BASE64 = "./data.base64";

    public static final String FILE_BIN = "./data.bin";

    public static final String FILE_JSON = "./data.json";

    public static final String FILE_XML = "./data.xml";

    public static final String FILE_JSON_FAST = "./data-fast.json";

    public static final String FILE_XML_FAST = "./data-fast.xml";
}