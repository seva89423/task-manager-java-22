package ru.zorin.tm.api.service;

import ru.zorin.tm.entity.Project;

import java.util.List;

public interface IProjectService extends IService<Project>{

    void create(String userId, String name);

    void create(String userId, String name, String description);

    void add(String userId, Project project);

    void remove(String userId, Project project);

    List<Project> findAll(String userId);

    void clear(String userId);

    Project findProjectById(String userId, String id);

    Project findProjectByIndex(String userId, Integer index);

    Project findProjectByName(String userId, String name);

    Project removeProjectByName(String userId, String name);

    Project removeProjectByIndex(String userId, Integer index);

    Project removeProjectById(String userId, String id);

    Project updateProjectById(String userId, String id, String name, String description);

    Project updateProjectByIndex(String userId, Integer index, String name, String description);
}
