package ru.zorin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.entity.AbstractEntity;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    @NotNull
    List<E> findAll();

    E add (E entity);

    void clear();

    @Nullable
    void add(@NotNull List<E> eList);

    @Nullable
    void load(@NotNull List<E> eList);

}