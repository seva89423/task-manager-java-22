package ru.zorin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.entity.User;
import ru.zorin.tm.error.invalid.InvalidUserIdException;
import ru.zorin.tm.role.Role;
import ru.zorin.tm.util.HashUtil;
import ru.zorin.tm.util.TerminalUtil;

public class UserUpdateByIdCommand extends AbstractCommand {
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "update-user-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Update user by id";
    }

    @Nullable
    @Override
    public void execute() throws Exception {
        final String userId = IServiceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE USER]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final User user = IServiceLocator.getUserService().findById(id);
        if (user == null) throw new InvalidUserIdException();
        System.out.println("ENTER NEW LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        final User userUpdated = IServiceLocator.getUserService().updateById(id, login, HashUtil.salt(password));
        if (userUpdated == null) throw new InvalidUserIdException();
        System.out.println("[COMPLETE]");
    }

    @NotNull
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}