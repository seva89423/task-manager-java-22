package ru.zorin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.entity.Task;
import ru.zorin.tm.error.invalid.InvalidIndexException;
import ru.zorin.tm.error.task.TaskEmptyException;
import ru.zorin.tm.role.Role;
import ru.zorin.tm.util.TerminalUtil;

public class TaskRemoveByIndexCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "remove-task-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by index";
    }

    @Nullable
    @Override
    public void execute() throws Exception {
        final String userId = IServiceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER INDEX:");
        try {
            final Integer index = TerminalUtil.nextNumber() - 1;
            final Task task = IServiceLocator.getTaskService().removeOneByIndex(userId, index);
            if (task == null) throw new TaskEmptyException();
            if (index == null || index < 0) throw new InvalidIndexException();
            System.out.println("[COMPLETE]");
        } catch (IndexOutOfBoundsException e) {
            throw new InvalidIndexException();
        }
    }

    @NotNull
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }
}