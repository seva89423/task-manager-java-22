package ru.zorin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.entity.Task;
import ru.zorin.tm.error.task.TaskEmptyException;
import ru.zorin.tm.role.Role;
import ru.zorin.tm.util.TerminalUtil;

public class TaskRemoveByNameCommand extends AbstractCommand {
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "remove-task-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by name";
    }

    @Nullable
    @Override
    public void execute() throws Exception {
        final String userId = IServiceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = IServiceLocator.getTaskService().removeOneByName(userId, name);
        if (task == null) throw new TaskEmptyException();
        System.out.println("[COMPLETE]");
    }
    @NotNull
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }
}