package ru.zorin.tm.command;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.zorin.tm.api.service.IServiceLocator;
import ru.zorin.tm.role.Role;

public abstract class AbstractCommand {

    @Setter
    protected IServiceLocator IServiceLocator;

    public AbstractCommand() {

    }

    public Role[] roles(){
        return null;
    }

    public abstract String arg();

    @NotNull
    public abstract String name();

    @NotNull
    public abstract String description();

    @NotNull
    public abstract void execute() throws Exception;
}