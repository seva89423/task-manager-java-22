package ru.zorin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.entity.Project;
import ru.zorin.tm.error.invalid.InvalidIndexException;
import ru.zorin.tm.error.project.ProjectEmptyException;
import ru.zorin.tm.role.Role;
import ru.zorin.tm.util.TerminalUtil;

public class ProjectRemoveByIndexCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "remove-project-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project using index";
    }

    @Nullable
    @Override
    public void execute() throws Exception {
        final String userId = IServiceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER INDEX:");
        try {
            final Integer index = TerminalUtil.nextNumber() - 1;
            final Project project = IServiceLocator.getProjectService().removeProjectByIndex(userId, index);
            if (project == null) throw new ProjectEmptyException();
            else System.out.println("[COMPLETE]");
        } catch (IndexOutOfBoundsException e) {
            throw new InvalidIndexException();
        }
    }

    @NotNull
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }
}