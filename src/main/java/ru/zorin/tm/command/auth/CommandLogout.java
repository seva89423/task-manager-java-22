package ru.zorin.tm.command.auth;

import ru.zorin.tm.command.AbstractCommand;

public class CommandLogout extends AbstractCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "logout";
    }

    @Override
    public String description() {
        return "Log out the system";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOGOUT]");
        IServiceLocator.getAuthService().logout();
        System.out.println("[COMPLETE]");
    }
}